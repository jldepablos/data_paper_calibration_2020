# README #

The original data shown within this repository have been used for the creation of the research paper entitled 
"Calibration of complex constitutive material models using meta-models and Gaussian processes" and has been
made freely available to any reader interested in the work presented therein. 
Please, use it responsibly.

### Structure ###
The structure shown for the available data within this repository corresponds to that employed
in the creation of the different "Results" subsections presented in the article.

### Who do I talk to? ###

If you’d like to contact the authors, please reach out via email (juanluis.pablos@imdea.org).
